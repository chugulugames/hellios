/*******************************************************************************
 *  @file		TwitterPopup.h
 *  @brief		HelliOs 
 *  @author		François Benaiteau
 *  @version	1.0
 *  @date		8/16/11
 *
 *  Copyright 	Chugulu Games 2009-2011. All rights reserved.
 *******************************************************************************/

#import <UIKit/UIKit.h>
#import "TwitterPopupDelegate.h"

@interface TwitterPopup : UIView<TwitterPopupDelegate> {
    id<TwitterPopupDelegate>    _delegate;
}

@end
