/*******************************************************************************
 *  @file		TwitterPopup.m
 *  @brief		HelliOs 
 *  @author		François Benaiteau
 *  @version	1.0
 *  @date		8/16/11
 *
 *  Copyright 	Chugulu Games 2009-2011. All rights reserved.
 *******************************************************************************/

#import "TwitterPopup.h"


@implementation TwitterPopup

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
