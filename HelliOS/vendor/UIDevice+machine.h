//
//  UIDevice+machine.h
//  blind_test
//
//  Created by Olivier THIERRY on 12/10/10.
//  Copyright 2010 Chugulu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIDevice;

@interface UIDevice (machine)

- (NSString *)machine;

@end
