/**
 @file		HelliOS.h
 @brief		HelliOS 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.

 Hellios root class.
 Hellios is a singleton, it represents a container for easy access to your 
 app external ressources. (Providers, Settings... and much more soon !)
 
 @mainpage HelliOS project
 
 \section Changelog Changelog

 \subsection version_1_3_0 v1.3.0 le 12/08/2011
 - Modules!
 - Twitter git submodule
 - Facebook git submodule
 - MBProgressHud git submodule
 - New Restkit Client for notifications
 
 
 \subsection version_1_2_0 v1.2.0 le 25/05/2011
 - Network module enhancement
 - Created HTTPContext whitch auto-observe reachability
 - Created HORequestQueue whitch handles multiple async. requests
 - Created HONotificationViewController (to use instead of HONotificationView)
 - Created HOResponse object whitch subclass NSURLResponse and provide additional methods
 - HORequest enhancement
 - HORequest & HOResponse are linked to their assigned context so we can access any of them easily
 
 - HONotificationView deprecated
 
 \subsection version_1_1_0 v1.1.0 le 25/05/2011
 - Re-make cache module from scratch
 - Added cache object class (HOCacheObject) to implement NSCoding by default
 - Provide notification (Pull) integration (=> Created HONotificationCenter, HONotification, HONotificationView) 
 - Added HODelegate to perform call backs very easily whitout creating/conforming to a protocol
 - Created XML parser (SAX) (Will be usefull for RSS feeds parsing : XML)
 - Updated social networks libraries & SDK (Facebook (ext.), Twitter (ext.), Skyrock (int.))
 - Integrated MPOAuthAPI
 - Updated Reachability
 - Renamed HOParser to HODataConverter
 - Cache module
 - HOiTunes to communicate with iTunes search API
 - HOParser whitch convert : Binary <=> JSON, Binary <=> Plist etc...

 \subsection version_1 v1.0.0 le 15/04/2011
 - HelliOS structure and settings (initialization)
 - HelliOS first reusable snippets integration (+transformations for generic code)
 - 1 line of code Application rating 
 - Generic splash view controller
 - HOEncoder to generate md5, encode url etc...
 - Controller loader, localization wrapper
 - Apple classes extentions
 - Provider communication (HOProvider, HORequest)
 - social networks
 - other stuff
*/

/**
 * HelliOS
 */
#import "HOSettings.h"
#import "HOMacros.h"
#import "HelliOSDelegateProtocol.h"

#import "HONotificationCenterHTTPClientProtocol.h"
#ifdef __HO_LIB__
#import "HONotificationCenterHTTPClientBase.h"
#else
#import "Notifications/HONotificationCenterHTTPClientBase.h"
#endif
/**
 * Snippets
 */
#import "HOErrorHandler.h"
#import "HOAppStoreLink.h"
#import "HOAppRating.h"
#import "HODelegate.h"
#import "HOSplashViewController.h"
#import "HOViewController.h"
#import "HOEncoder.h"
#import "HOiTunes.h"
#import "HODataConverter.h"
#import "HOXMLParser.h"

#ifdef __FRAMEWORK__
#import "Network.h"
#import "ObjectStorage.h"
#import "Notifications.h"
#import "I18n.h"
#endif

#pragma mark - Interface declaration


@interface HelliOS : NSObject {

@private
    id<HelliOSDelegate>                  _delegate;
    HOSettings                          *_settings;
}


#pragma mark - Properties


/**
 * @brief The main delegate of HelliOS
 *
 * @note  Should be your application delegate
 */
@property (nonatomic, assign)               id<HelliOSDelegate>                delegate;


/**
 * @brief Application's settings
 */
@property (nonatomic, readonly, retain)     HOSettings                        *settings;

#pragma mark - Methods


/**
 @brief HelliOS version
*/
+ (NSString*)   version;
/**
 @brief HelliOS version with details (build date, revision...)
 */
+ (NSString*)   versionFull;

/**
 @brief HelliOS instance

 @note  you should NOT need to call this method.
        Use static methods instead.
        HelliOS internal classes use it.
*/
+ (HelliOS*)    sharedInstance;


/**
 @brief this method initialize HelliOS instance.
 
 @param NSDictionary*          settings    : the object to cache
 @param id<HelliOSDelegate>    delegate    : the key to retreive the object in cache
 
 @note  You should call this method right after you're app has been launched.
        You MUST call this method before doing anything else with HelliOS
        otherwise an HelliOSException will be raised
*/
+ (void)        initializeWithSettings:(NSDictionary*)settings 
                              delegate:(id<HelliOSDelegate>)delegate;

@end
