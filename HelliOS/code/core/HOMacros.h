//
//  HOMacros.h
//  HelliOS
//
//  Created by Olivier Thierry on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef HelliOS_HOMacros_h
#define HelliOS_HOMacros_h

#define HO_SAFE_RELEASE(object) [object release]; object = nil;

#endif
