/*******************************************************************************
 *  @file		HOXMLNode.m
 *  @brief		HOFeedFetcher 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/5/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOXMLNode.h"


@implementation HOXMLNode

@synthesize parent  = _parent, 
            name    = _name, 
            objects = _objects, 
            content = _content;

#pragma mark --
#pragma mark Initialization

- (id) initWithName:(NSString*)nodeName 
{
    self = [super init];
    
    if (self) {
        _name    = [nodeName retain];
        _objects = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark --
#pragma mark Memory managment

- (void) dealloc
{
    [_objects release]; _objects = nil;
    [_content release]; _content = nil;
    [_name release];    _name    = nil;
    [super dealloc];
}

@end
