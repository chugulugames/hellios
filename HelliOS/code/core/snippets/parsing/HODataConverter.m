//
//  HODataConverter.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HODataConverter.h"
#import "SBJSON.h"

@implementation HODataConverter

+ (id) JSONFromString:(NSString*)string {
    
    SBJsonParser *parser = nil;
    id retVal            = nil;
    
    parser = [[SBJsonParser alloc] init];
    retVal = [parser objectWithString:string];
    [parser release];
    
    return retVal;
   
}

+ (id) JSONFromData:(NSData*)data {
    
    id retVal = nil;
    
    NSString *dataToString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    retVal = [self JSONFromString:dataToString];
    [dataToString release];
    
    return retVal;
}

+ (NSString*)     stringFromJSON:(id)JSON {

    SBJsonWriter *writer       = nil;
    NSString *stringFromJSON   = nil;
    
    writer                     = [[SBJsonWriter alloc] init];
    
    stringFromJSON             = [writer stringWithObject:JSON];
    
    [writer release];
    
    return stringFromJSON;
}

+ (NSData*)       dataFromJSON:(id)JSON 
{    
    return [[self stringFromJSON:JSON] dataUsingEncoding:NSUTF8StringEncoding];
}

+ (id)            plistFromData:(NSData*)data {
    
    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 4.0) {  
        return [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:NULL error:NULL];
    }
    
    return [NSPropertyListSerialization propertyListFromData:data mutabilityOption:0 format:NULL errorDescription:NULL];

}

+ (NSData*)       dataFromPlist:(id)plist {
 
    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 4.0) {  
        return [NSPropertyListSerialization dataWithPropertyList:plist format:NSPropertyListBinaryFormat_v1_0 options:0 error:NULL];
    }
    
    return [NSPropertyListSerialization dataFromPropertyList:plist format:NSPropertyListBinaryFormat_v1_0 errorDescription:NULL];

}

@end
