//
//  HOiTunes.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SBJSON.h"
#import "HOiTunes.h"

@implementation HOiTunes

+ (NSDictionary*) getResultsFromItunesWithQuery:(NSString*)query 
{    
	SBJsonParser        *parser                 = nil;
    NSDictionary		*fetchedJSON            = nil;
	NSString			*cleanQuery             = nil;
	NSURL				*itunesFormatedQueryURL = nil;	
    
    parser                                      = [[SBJSON alloc] init];
    
	cleanQuery                                  = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, 
                                                                                                       (CFStringRef)[query stringByReplacingOccurrencesOfString:@" " withString:@"+"], 
                                                                                                       CFSTR("+"), 
                                                                                                       CFSTR(":/?#[]@!$&’()*,;="),
                                                                                                       kCFStringEncodingUTF8);
	
	itunesFormatedQueryURL                      = [NSURL URLWithString:[NSString stringWithFormat:@"http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/wa/wsSearch?term=%@&country=%@&media=music&limit=1", cleanQuery , [[NSLocale autoupdatingCurrentLocale] objectForKey:NSLocaleCountryCode]]];
    
	fetchedJSON                                 = [parser objectWithString:[NSString stringWithContentsOfURL:itunesFormatedQueryURL encoding:NSUTF8StringEncoding error:NULL]];
    
    [parser release];
    
    return fetchedJSON;
    
}

+ (NSURL*) getProductURLFromItunesWithQuery:(NSString*)query
{    
    NSDictionary		*fetchedJSON = nil;
    
    fetchedJSON                      = [self getResultsFromItunesWithQuery:query];
    
    if ([[fetchedJSON objectForKey:@"results"] count]) 
    {
        return [NSURL URLWithString:[[[fetchedJSON objectForKey:@"results"] objectAtIndex:0] objectForKey:@"trackViewUrl"]];
    }
    
    return nil;
}

+ (void) launchItunesWithQuery:(NSString*)query 
{
    [[UIApplication sharedApplication] openURL:[self getProductURLFromItunesWithQuery:query]];
}

@end
