//
//  HOSplash.m
//  HelliOS
//
//  Created by Sergio Kunats on 3/26/12.
//  Copyright (c) 2012 Chugulu. All rights reserved.
//

#import "HOSplash.h"

NSString* const HOSplashUntimedSplashEndSelectorName = @"contentSequenceDidFinish";

@implementation HOSplash

@synthesize splash;
@synthesize splashView;
@synthesize duration;
@synthesize observable;
@synthesize finished;

- (id) initWithSplash:(id)someSplash view:(UIView *)someSplashView duration:(NSTimeInterval)someDuration {
    if ((self = [super init])) {
        self.splash     = someSplash;
        self.splashView = someSplashView;
        self.duration   = someDuration;
    }
    return self;
}

- (void) dealloc {
    self.splash     = nil;
    self.splashView = nil;
    [super dealloc];
}

- (BOOL) isObservable {
    return [self.splash respondsToSelector:NSSelectorFromString(HOSplashUntimedSplashEndSelectorName)];
}

- (BOOL) isFinished {
    return [self isObservable] && [self.splash performSelector:NSSelectorFromString(HOSplashUntimedSplashEndSelectorName)];
}

@end
