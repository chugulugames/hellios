//
//  HOViewController.m
//  HelliOS
//
//  Created by Olivier THIERRY on 5/10/11.
//  Copyright 2011 Chugulu. All rights reserved.
//

#import "HOViewController.h"

@implementation HOViewController

- (void)viewDidLoad 
{
    [super viewDidLoad];	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return [[HelliOS sharedInstance].settings.supportedInterfaceOrientations containsObject:[NSNumber numberWithInt:interfaceOrientation]];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}


@end
