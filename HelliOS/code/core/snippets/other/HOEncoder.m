//
//  HOEncoder.m
//  Chugulu
//
//  Created by Olivier THIERRY on 3/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HOEncoder.h"


@implementation HOEncoder

+ (NSString *) encodeStringToMD5:(NSString *) inputStr
{
	NSData* inputData = [inputStr dataUsingEncoding:NSUTF8StringEncoding];
	unsigned char outputData[CC_MD5_DIGEST_LENGTH];
	CC_MD5([inputData bytes], [inputData length], outputData);
	
	NSMutableString* hashStr = [NSMutableString string];
	int i = 0;
	for (i = 0; i < CC_MD5_DIGEST_LENGTH; ++i)
		[hashStr appendFormat:@"%02x", outputData[i]];
	
	return hashStr;
}

+ (NSURL*)     encodeURL:(NSString*)url {
    NSString *result = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)url, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8);
	return [NSURL URLWithString:result];
}

@end
