//
//  HODelegate.h
//  HelliOS
//
//  Created by Olivier THIERRY on 5/3/11.
//  Copyright 2011 Chugulu. All rights reserved.
//


@interface HODelegate : NSObject {
	id	   _target;
	id	   _object;
	SEL	   _selector;
}

@property (nonatomic, retain) id  target;
@property (nonatomic, retain) id  object;
@property (nonatomic)		  SEL selector;

+ (HODelegate*) delegateWithTarget:(id)target selector:(SEL)selector;
+ (HODelegate*) delegateWithTarget:(id)target selector:(SEL)selector object:(id)object;

- (void) perform;

@end
