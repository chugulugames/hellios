/**
 @file		HOAppRating.m
 @brief		HelliOS App Store App Rating prompt
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
*/

#import "HelliOS.h"
#import "HOAppRating.h"
#import "HOAppStoreLink.h"

@implementation HOAppRating

+ (void) showApplicationRatingProposal:(id<HOAppRatingDelegate>)ratingDelegate askNeverAsk:(BOOL)neverAsk {
    [[self class] showApplicationRatingProposal:ratingDelegate askNeverAsk:neverAsk withCustomText:NSLocalizedString(@"HORATING_PLEASE_RATE_ME", nil)];
}

+ (void) showApplicationRatingProposal:(id<HOAppRatingDelegate>)ratingDelegate askNeverAsk:(BOOL)neverAsk withCustomText:(NSString*)text {
    if (![HelliOS sharedInstance].settings.iTunesApplicationID) {
        [NSException raise:@"HelliOSApplicationRatingException" format:@"You must provide your itunes application ID when initializing HelliOS (Settings) Before using the built-in application rating."];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"HORATING_TITLE_FORMAT", nil), [HelliOS sharedInstance].settings.applicationName]
                                                        message:NSLocalizedString(text, nil)
                                                       delegate:ratingDelegate
                                              cancelButtonTitle:NSLocalizedString(@"HORATING_NOT_NOW", nil)
                                              otherButtonTitles:NSLocalizedString(@"HORATING_YES", nil), (neverAsk ? NSLocalizedString(@"HORATING_NEVER_ASK", nil) : nil), nil];

    [alertView show];
    [alertView release];
}

+ (BOOL) openAppStoreRatingPage {
    return [[UIApplication sharedApplication] openURL:[HOAppStoreLink viewContentsUserReviews:[HelliOS sharedInstance].settings.iTunesApplicationID]];
}

@end
