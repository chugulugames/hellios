/*******************************************************************************
 *  @file		HONotificationCenterHTTPClientProtocol.h
 *  @brief		HelliOs 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/26/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

@protocol HONotificationCenterHTTPClientProtocol <NSObject>

- (void) pull;

@end
