//
//  HOSettings.h
//  HelliOS
//
//  Created by Olivier THIERRY on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kHOSettingsApplicationName;
extern NSString* const kHOSettingsiTunesApplicationID;
extern NSString* const kHOSettingsSupportedInterfaceOrientations;
extern NSString* const kHOSettingsNotificationsURL;

#pragma mark - Interface declaration

@interface HOSettings : NSObject 
{
@private
    NSString            *_applicationName;
    NSString            *_iTunesApplicationID;
	NSArray             *_supportedInterfaceOrientations;
	NSURL               *_notificationsURL;
    NSMutableDictionary *_providers;
}

#pragma mark - Properties


/**
 * @brief Represents your application's name
 *
 * @see   kHOSettingsApplicationName
 */
@property (nonatomic, retain)   NSString *applicationName;


/**
 * @brief Represents your application's iTunes ID
 *
 * @see   kHOSettingsiTunesApplicationID
 */
@property (nonatomic, retain)   NSString *iTunesApplicationID;


/**
 * @brief Represents your application's supported interface orientations
 *  
 * @see   kHOSettingsSupportedInterfaceOrientations
 */
@property (nonatomic, retain)   NSArray  *supportedInterfaceOrientations;


/**
 * @brief Represents your application's pull notification's URL
 *
 * @see   kHOSettingsNotificationsURL
 */
@property (nonatomic, retain)   NSURL    *notificationsURL;

/**
 * @brief Application's providers
 *
 * @note  Indexed by name, use + (HOProvider*) [HOProvider providerForName:(NSString*)providerName]
 *        to access one of them
 */
@property (nonatomic, retain)   NSMutableDictionary               *providers;

/**
 * @brief sets all the settings from a dictionary
 */
- (void) load:(NSDictionary*)settings;

@end
