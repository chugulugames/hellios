//
//  HelliOS.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HelliOS.h"
#import "build_info.h"

@implementation HelliOS

static HelliOS *instance = NULL;

@synthesize delegate  = _delegate,
settings  = _settings;

#pragma mark - Initialization

- (id) init
{
    self = [super init];
    
    if (self) 
    {
        _settings  = [HOSettings new];
    }
    
    return self;
}

#pragma mark - Singleton

+ (HelliOS*)  sharedInstance 
{        
    @synchronized(self)
    {
        if (!instance) 
        {
            instance = [self new];
        }
    }
    
    return instance;
}

+ (id)allocWithZone:(NSZone *)zone
{ 
	@synchronized(self)
	{ 
		if (instance == nil) 
		{
			instance = [super allocWithZone:zone];
			return instance;
		}
	}
	return nil;
} 

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (id)retain
{
	return self;
} 

- (NSUInteger)retainCount
{
	return NSUIntegerMax;
}

- (void) dealloc
{
    // Do nothing
    // Singleton
}

- (void)release 
{ 
    // Do nothing
    // Singleton
} 

- (id)autorelease
{
	return self;
}

#pragma mark - Public

+ (NSString*) version {
    return @"1.3.2 alpha";
}

+ (NSString*) versionFull {
#ifdef BUILD_DATE
    return [NSString stringWithFormat:@"VERSION:\t\t%@\nBUILD DATE:\t\t%@\nREVISION:\t\tr%@", [[self class] version], BUILD_DATE, BUILD_REVISION];
#endif
    return [NSString stringWithFormat:@"VERSION:\t%@", [[self class] version]];
}

+ (void) initializeWithSettings:(NSDictionary*)settings 
                       delegate:(id<HelliOSDelegate>)delegate
{
	NSLog(@"\n<====[%@]====>\n%@\n<=================>", NSStringFromClass([self class]), [[self class] versionFull]);

	[[self class] sharedInstance].delegate = delegate;
    [[[self class] sharedInstance].settings load:settings];
}


@end
