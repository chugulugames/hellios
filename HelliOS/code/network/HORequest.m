/**
 @file		HORequest.m
 @brief		HORequest 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
 */

#import "HORequest.h"
#import "HORequestQueue.h"

@implementation HORequest

@synthesize parameters   = _parameters, 
            delegate     = _delegate,
            identifier   = _identifier, 
            context      = _context;

#pragma mark --
#pragma mark Initialization

- (id) init 
{
	if ((self = [super init])) {
        _built      = NO;
        _delegate   = nil;
        _context    = nil;
        _identifier = nil;
        _parameters = nil;
    }
	return self;
}

+ (HORequest*) request 
{
	return [[[self alloc] init] autorelease];	
}

#pragma mark --
#pragma mark memory management

-(void) dealloc 
{
    HO_SAFE_RELEASE(_delegate);
    HO_SAFE_RELEASE(_parameters);
    HO_SAFE_RELEASE(_identifier);
    [super dealloc];
}

#pragma mark - Public

- (void) setHeader:(NSDictionary*)header {
 
    NSMutableString *headerString = [[NSMutableString alloc] init];
    
    for (NSString *key in [header allKeys]) {
        
        if ([headerString length] > 0) 
        {
            [headerString appendString:@" "];
        }

        [headerString appendFormat:@"%@/%@", key, [header objectForKey:key]];
    }
    
   	[self setValue:headerString forHTTPHeaderField:@"User-Agent"]; 

    [headerString release];
}

- (void) build 
{
	if (_built || _parameters == nil)
        return;
    _built = YES;
    NSMutableString *params = [[NSMutableString alloc] init];
    
    for (NSString *key in [_parameters allKeys]) 
    {
        if ([params length] > 0) 
            [params appendString:@"&"];
        
        [params appendFormat:@"%@=%@", key, [_parameters objectForKey:key]];
    }
    
    if([[self HTTPMethod] isEqualToString:@"GET"]) 
    {            
        NSString * urlWithParams = [NSString stringWithFormat:@"%@?%@", [[self URL] relativeString], params];
        NSLog(@"URL GET Request : %@", urlWithParams);            
        [self setURL:[NSURL URLWithString:urlWithParams]];
    }
    else
    {
        [self setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [self setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [params release];
}

- (void) startAsynchronously 
{
    [self build]; 
    [[HORequestQueue sharedQueue] addRequest:self];
}

- (id) startSynchronously 
{
    [self build];
    return [[HOHTTPContext HTTPContextWithRequest:self] loadSynchronously];
}

@end
