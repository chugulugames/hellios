/*******************************************************************************
 *  @file		HOResponse.m
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/22/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOResponse.h"

@implementation HOResponse

@synthesize context = _context,
            status  = _status,
            error   = _error;

#pragma mark --
#pragma mark Initialization

- (id) initWithURL:(NSURL *)URL 
          MIMEType:(NSString *)MIMEType 
expectedContentLength:(NSInteger)length
  textEncodingName:(NSString *)name
            status:(NSUInteger)status
           context:(HOHTTPContext*)context;
{
    self = [super initWithURL:URL MIMEType:MIMEType expectedContentLength:length textEncodingName:name];
    
    if (self) 
    {
        _status  = status;
        _context = context;
    }
    
    return self;
}

#pragma mark --
#pragma mark Memory management

- (void) dealloc
{
    HO_SAFE_RELEASE(_error);
    [super dealloc];
}

#pragma mark --
#pragma mark Public method

- (HOResponseType) type;
{
    if ([[self MIMEType] isEqualToString:@"application/json"])
    {
        return HOResponseTypeJSON;
    }
    else if ([[self MIMEType] isEqualToString:@"text/xml"])
    {
        return HOResponseTypePlist;
    }
    
    return HOResponseTypeUnknow;
}

@end
