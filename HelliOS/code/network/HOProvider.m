//
//  HOProvider.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HOProvider.h"
#import "HelliOS.h"

@implementation HOProvider

@synthesize name                    = _name, 
            baseURL                 = _baseURL,
            requestsFormatedHeader  = _requestsFormatedHeader;

+ (void) setProviders:(NSArray*)providers
{
    NSMutableDictionary* providerDictionary = [NSMutableDictionary new];
	for (HOProvider *provider in providers)
        [providerDictionary setObject:provider forKey:provider.name];

    [HelliOS sharedInstance].settings.providers = providerDictionary;
    [providerDictionary release];
}

+ (HOProvider*) providerForName:(NSString*)providerName 
{
    return [[HelliOS sharedInstance].settings.providers objectForKey:providerName];
}

- (id) initWithName:(NSString*)name baseServiceURL:(NSString*)baseURL requestsFormatedHeader:(NSDictionary*)header;
{
    self = [super init];
    
    if (self) {
        
        if (name == nil || baseURL == nil) {
            [NSException raise:@"HOProviderInitializationException" format:@"Neither the name nor the base URL parameters can be nil"];
        }
        
        _name                   = [name retain];
        _baseURL                = [baseURL retain];
        _requestsFormatedHeader = [header retain];
    }
    
    return self;
}

- (id) initWithName:(NSString*)name baseServiceURL:(NSString*)baseURL 
{
    return [self initWithName:name baseServiceURL:baseURL requestsFormatedHeader:nil];
}

+ (id) providerWithName:(NSString*)name baseServiceURL:(NSString*)baseURL requestsFormatedHeader:(NSDictionary*)header
{
    return [[[self alloc] initWithName:name baseServiceURL:baseURL requestsFormatedHeader:header] autorelease];
}

+ (id) providerWithName:(NSString*)name baseServiceURL:(NSString*)baseURL
{
    return [self providerWithName:name baseServiceURL:baseURL requestsFormatedHeader:nil];
}

- (HORequest*) getRequestForServiceWithPath:(NSString*)path 
                                 parameters:(NSDictionary*)parameters
                                 HTTPMethod:(NSString*)httpMethod
{
    HORequest *request           = [HORequest request];    
    request.URL                  = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", _baseURL, path]];
    request.parameters           = parameters;
    request.HTTPMethod           = httpMethod;
    
    if (_requestsFormatedHeader != nil) 
    {
        [request setHeader:_requestsFormatedHeader];
    }    
    
    [request build];
    
    return request;
}

- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                      delegate:(id<HORequestDelegate>)delegate
{
    [self performAsynchronousRequestForServiceWithPath:path 
                                            parameters:parameters
                                            HTTPMethod:httpMethod 
                                            identifier:nil 
                                              delegate:delegate];
}

- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                    identifier:(NSString*)requestIdentifier
                                                      delegate:(id<HORequestDelegate>)delegate
{
    HORequest *request              = [self getRequestForServiceWithPath:path 
                                                              parameters:parameters 
                                                              HTTPMethod:httpMethod];
    request.delegate                = delegate;
    request.identifier              = requestIdentifier;
 
    // since ios 4.0 NSURLConnection doesn't start loading data if the current thread is not the MAIN thread !
    // so we perform the start method on the main thread ! Be carefull with that !
    [request performSelectorOnMainThread:@selector(startAsynchronously) withObject:nil waitUntilDone:NO];
}

- (id)            performSynchronousRequestForServiceWithPath:(NSString*)path 
                                                   parameters:(NSDictionary*)parameters
                                                   HTTPMethod:(NSString*)httpMethod
{
    HORequest *request           = [self getRequestForServiceWithPath:path 
                                                              parameters:parameters 
                                                              HTTPMethod:httpMethod];
    
    return [request startSynchronously];
}

- (void) dealloc 
{
    [_name release];
    [_baseURL release];
    [_requestsFormatedHeader release];
    [super dealloc];
}


@end
