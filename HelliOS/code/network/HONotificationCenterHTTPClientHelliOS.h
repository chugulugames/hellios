/*******************************************************************************
 *  @file		HONotificationCenterHTTPClient.h
 *  @brief		HelliOs 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/26/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#ifdef __HO_LIB__
#import "HORequest.h"
#import "HONotificationCenterHTTPClientBase.h"
#else
#import "HORequest.h"
#import "../Notifications/HONotificationCenterHTTPClientBase.h"
#endif

@interface HONotificationCenterHTTPClientHelliOS : HONotificationCenterHTTPClientBase<HORequestDelegate> {

}

@end
