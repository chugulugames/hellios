/**
 *  @file		HORequestQueue.h
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/30/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 */

#import <Foundation/Foundation.h>
#ifdef __HO_LIB__
#import "Reachability.h"
#import "HOHTTPContext.h"
#else
#import "../Support/Reachability.h"
#import "HOHTTPContext.h"
#endif

@interface HORequestQueue : NSObject
{
@private
    NSMutableArray      *_requests;
    BOOL                _loading;
}

@property (nonatomic, readonly) NSArray         *requests;

/**
 * @brief  this method returns the shared instance of the request queue
 * @return HORequestQueue* the shared instance
 */
+ (HORequestQueue*) sharedQueue;


/**
 * @brief this method add a request in the requests queue
 * @param HORequest* the request to add in the queue
 */
- (void)            addRequest:(HORequest*)request;


/**
 * @brief this method remove a request from requests queue
 * @param HORequest* the request to remove from the queue
 */
- (void)            removeRequest:(HORequest*)request;

@end
