/**
 *  @file		HOResponse.h
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/22/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "HORequest.h"

@class HOHTTPContext;

/**
 @brief HOResponseType
 */
enum HOResponseType {
    HOResponseTypeJSON,
    HOResponseTypePlist,
    HOResponseTypeBinary,
    HOResponseTypeUnknow
};
typedef enum HOResponseType HOResponseType;


#pragma mark - Interface declaration


@interface HOResponse : NSURLResponse 
{
    HOHTTPContext     *_context;
    
    NSUInteger         _status;
    NSError           *_error;
}


#pragma mark - Properties


/**
 * @brief the HTTPContext whitch is attached to the response
 *
 * @see   HOHTTPContext.h/m
 */
@property (nonatomic, assign)  HOHTTPContext *context;


/**
 * @brief The status code of the response
 */
@property (nonatomic, readonly)  NSUInteger     status;


/**
 * @brief The status code of the response
 */
@property (nonatomic, retain)    NSError       *error;



#pragma mark - Methods



- (id) initWithURL:(NSURL *)URL 
          MIMEType:(NSString *)MIMEType 
expectedContentLength:(NSInteger)length
  textEncodingName:(NSString *)name
            status:(NSUInteger)status
           context:(HOHTTPContext*)context;


/**
 * @brief Indicate the type of the response. It could be JSON, xml, etc...
 *
 * @see   HOResponseType (enum)
 */
- (HOResponseType) type;

@end
