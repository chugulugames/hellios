//
//  HOCache.m
//  blind_test
//
//  Created by Olivier THIERRY on 11/5/10.
//  Copyright 2010 Chugulu. All rights reserved.
//

#import "HOCache.h"

@implementation HOCache

+ (void) cacheObject:(id<NSObject,NSCoding>)object forKey:(NSString*)name
{
    [object retain];
    NSAutoreleasePool* pool = [NSAutoreleasePool new];
	NSString *cachesPath      = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	
	NSString *path            = [cachesPath stringByAppendingFormat:@"/%@.archive", name];
       
	if ([NSKeyedArchiver archiveRootObject:object toFile:path]) 
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSMutableArray *updatedKeys = [NSMutableArray arrayWithArray:[defaults arrayForKey:@"hellios.cache.objectKeys"]];
		[updatedKeys addObject:name];		
		[defaults setObject:updatedKeys forKey:@"hellios.cache.objectKeys"];
		[defaults synchronize];
	}
    [pool release];
    HO_SAFE_RELEASE(object);
}

+ (id) cachedObjectForKey:(NSString*)key 
{
	NSString *cachesPath	   = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];

	NSString *cachedObjectPath = [cachesPath stringByAppendingFormat:@"/%@.archive", key];
  
	return [NSKeyedUnarchiver unarchiveObjectWithFile:cachedObjectPath];	
}

+ (NSArray*) cachedObjectsWithKeyPrefix:(NSString*)keyPrefix
{
    NSMutableArray *fetchedObjects = [NSMutableArray array];
    
    for (NSString *key in [[NSUserDefaults standardUserDefaults] arrayForKey:@"hellios.cache.objectKeys"]) 
	{
		if ([key hasPrefix:keyPrefix]) 
		{
            id object = [self cachedObjectForKey:key];
            if (object) 
            {
                [fetchedObjects addObject:object]; 
            }
		}
	}	
    
    return fetchedObjects;
}

+ (NSArray*) cachedObjectKeys
{
    return [[NSUserDefaults standardUserDefaults] arrayForKey:@"hellios.cache.objectKeys"];
}

+ (void) removeCachedObjectForKey:(NSString*)key {
    
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
	NSString *cachedDataPath = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/%@.archive", key]];
    
    NSDictionary *cachedDataInfo = [NSDictionary dictionaryWithContentsOfFile:cachedDataPath];
    
    if (cachedDataInfo != nil) {
        [[NSFileManager defaultManager] removeItemAtPath:cachedDataPath error:NULL];
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSMutableArray *updatedKeys = [NSMutableArray arrayWithArray:[defaults arrayForKey:@"hellios.cache.objectKeys"]];
		[updatedKeys removeObject:key];
		[defaults setObject:updatedKeys forKey:@"hellios.cache.objectKeys"];
		[defaults synchronize];
    }
}

+ (void) removeCachedObjectsForKeys:(NSArray*)keys
{
	for (NSString *key in keys) 
	{
		[self removeCachedObjectForKey:key];
	}
}

+ (void) removeCachedObjectsWithKeyPrefix:(NSString*)keyPrefix
{	
	for (NSString *key in [[NSUserDefaults standardUserDefaults] arrayForKey:@"hellios.cache.objectKeys"]) 
	{
		if ([key hasPrefix:keyPrefix]) 
		{
			[self removeCachedObjectForKey:key];
		}
	}	
}

+ (void) clear
{
	NSArray *cachedObjectKeys = [[NSUserDefaults standardUserDefaults] arrayForKey:@"hellios.cache.objectKeys"];
	
    for (NSString *key in cachedObjectKeys) 
    {
        [self removeCachedObjectForKey:key];
    }		
}

@end
