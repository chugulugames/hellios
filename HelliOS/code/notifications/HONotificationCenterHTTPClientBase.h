/*******************************************************************************
 *  @file		HONotificationCenterHTTPClientBase.h
 *  @brief		HelliOs 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/26/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#ifdef __HO_LIB__
#import "HONotificationCenterHTTPClientProtocol.h"
#else
#import "../HONotificationCenterHTTPClientProtocol.h"
#endif

@class HONotificationCenter;

@interface HONotificationCenterHTTPClientBase : NSObject<HONotificationCenterHTTPClientProtocol> {
    HONotificationCenter* _deliveryDelegate;
}

- (NSURL*) notificationsURL;
- (id) initWithDeliveryDelegate:(HONotificationCenter*)center;
- (void) deliver:(NSArray*)objects;

@end
