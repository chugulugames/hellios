/**
 *  @file		HONotificationViewController.h
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/8/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *
 *  @brief This class provides everythingyou need to display a notification.
 *         The notification is displayed using default pop-up.
 *         The pop up display the notification's title at the top, the message
 *         in the middle.
 *         You can assign delegates to handle the fact that the user click on "OK"
 *         or close the notification.
 */

#import <UIKit/UIKit.h>

#ifdef __HO_LIB__
#import "HelliOS.h"
#import "HONotification.h"
#else
#import "../HelliOS.h"
#import "HONotification.h"
#endif

#pragma mark - Interface declaration


@interface HONotificationViewController : UIViewController 
{
@private    
    // View components
    UIView                      *_notificationContainerView;
    UILabel                     *_notificationTitleLabel;
    UITextView                  *_notificationDescriptionLabel;
    UIButton                    *_notificationAcceptButton;
    NSString                    *_notificationAcceptButtonLabelText;

    // Model
	HONotification				*_notification;	
    
    // Event delegates
	HODelegate					*_onAccept;
	HODelegate					*_onDismiss;   
}


#pragma mark - properties


@property (nonatomic, retain) IBOutlet  UIView          *notificationContainerView;
@property (nonatomic, retain) IBOutlet  UILabel         *notificationTitleLabel;
@property (nonatomic, retain) IBOutlet  UITextView      *notificationDescriptionLabel;
@property (nonatomic, retain) IBOutlet  UIButton        *notificationAcceptButton;
@property (nonatomic, copy)             NSString        *notificationAcceptButtonLabelText;


/**
 * @brief The notification object model
 */
@property (nonatomic, retain)           HONotification            *notification;


/**
 * @brief The delegate that will be performed if the user click on "OK" button
 *        
 * @note  Can be nil.
 *
 * @see   HODelegate.h/m
 */
@property (nonatomic, retain)           HODelegate                *onAccept;


/**
 * @brief The delegate that will be performed if the user closes the notification
 *        
 * @note  Can be nil.
 *
 * @see   HODelegate.h/m
 */
@property (nonatomic, retain)           HODelegate                *onDismiss;


#pragma mark - Methods


- (id) initWithNotification:(HONotification*)notification;
+ (id) controllerWithNotification:(HONotification*)notification;

/**
 * @brief Shows the notification on the main window of the application
 */
- (void)show;


/**
 * @brief hides the notification
 */
- (void)hide;


/**
 * @brief The action whitch is called if the user closes the notification
 */
- (IBAction)dismiss;


/**
 * @brief The action whitch is called if the user click on "OK"
 */
- (IBAction)accept;

@end
