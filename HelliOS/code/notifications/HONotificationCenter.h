/**
 * @file		HONotificationCenter.h
 * @author      Olivier Thierry
 * @version     1.0
 * @date		25/04/2011
 *
 *
 * @brief		This object is the controller for pull notifications.
 *              It pulls notifications and handle HONotification instanciation.
 *              It also delivers HONotification objects to your application's delegate
 */

#import <UIKit/UIKit.h>
#ifdef __HO_LIB__
#import "HelliOS.h"
#import "HONotification.h"
#import "HONotificationCenterDelegateProtocol.h"
#import "HONotificationCenterHTTPClientProtocol.h"
#else
#import "../HelliOS.h"
#import "HONotification.h"
#import "HONotificationCenterDelegateProtocol.h"
#import "../HONotificationCenterHTTPClientProtocol.h"
#endif

#pragma mark - interface declaration


@interface HONotificationCenter : NSObject
{
	NSMutableArray                              *_deliveredNotificationIDs;
	NSTimer                                     *_pullTimer;
    id<HONotificationCenterDelegateProtocol>    _delegate;
    id<HONotificationCenterHTTPClientProtocol>  _notificationHttpClient;
}

@property (nonatomic, retain) id<HONotificationCenterDelegateProtocol> delegate;
@property (nonatomic, retain) id<HONotificationCenterHTTPClientProtocol> notificationHttpClient;

#pragma mark - Methods


/**
 * @brief   Singleton object accessor
 *
 * @return  HONotificationCenter* : the unique instance of HONotificationCenter 
 */
+ (HONotificationCenter*) sharedNotificationCenter;


/**
 * @brief Pulls notification using the kHOSettingsNotificationsURL from the HelliOS settings.
 *        If new notifications are pulled, they are next delivered to your application delegate
 *
 * @note  You must provide the URL used to pull notifications. You provide this url in the settings
 *        at HelliOS initialization time (kHOSettingsNotificationsURL)
 *
 * @see   Wiki Chapter "Notifications"
 * @see   HOSettings.h/m, kHOSettingsNotificationsURL
 */
- (void)                  pull;


/**
 * @brief Use this methods to schedule automatic pulls every x seconds
 *
 * @param (NSTimeInterval)timeInterval : The number of seconds between firings pulls
 *
 * @note  When you schedule an automatic pull and then you manually pull using
 *        - (void) pull method. The timer for next automatic pull restarts
 *        to avoid double pull.
 */
- (void)				  schedulePullWithTimeInterval:(NSTimeInterval)timeInterval;


/**
 * @brief Use this method to unschedule automatics pulls
 */
- (void)				  unschedulePull;

/**
 * @brief sends notification objects to the delegate
 */
- (void) deliver:(HONotification*)notification;

/**
 * @brief checks if the notification is already delivered
 */
- (BOOL)			didDeliverNotificationWithID:(NSString*)ID;

@end
