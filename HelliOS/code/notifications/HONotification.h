/**
 * @file		HONotification.h
 * @author      Olivier Thierry
 * @version     1.0
 * @date		25/04/2011
 *
 *
 * @brief		This model object represents a notification.
*/

#import <Foundation/Foundation.h>
#ifdef __HO_LIB__
#import "HODelegate.h"
#import "HOCacheObject.h"
#else
#import "../HODelegate.h"
#import "../ObjectStorage/HOCacheObject.h"
#endif

#pragma mark Interface declaration


@interface HONotification : HOCacheObject 
{
@private
	NSString				*_ID;
	NSString				*_title;
	NSString				*_message;
	NSDate                  *_publishDate;
    NSTimeInterval           _TTL;
	NSInteger				 _type;
	NSDictionary			*_attachedObjects;
    NSNumber                *_isAlreadyRead;
}


#pragma mark - Properties


/**
 * @brief The notification ID
 */
@property (nonatomic, retain) NSString		  *ID;


/**
 * @brief The title of the notification
 */
@property (nonatomic, retain) NSString		  *title;


/**
 * @brief The notification's message
 */
@property (nonatomic, retain) NSString		  *message;


/**
 * @brief The date the notification was published by your provider
 */
@property (nonatomic, retain) NSDate          *publishDate;


/**
 * @brief The time to live of the notification
 */
@property (nonatomic, assign) NSTimeInterval  TTL;


/**
 * @brief An integer representing the notification's type.
 *
 * @note  You should define your own notification types in your application
 *        using enumeration for clarity reasons.
 */
@property (nonatomic)		  NSInteger	   	  type;


/**
 * @brief The attached objects in the notification. These are additional objects
 *        that could be urls, arrays, dictionary, etc...
 */
@property (nonatomic, retain) NSDictionary    *attachedObjects;


/**
 * @brief Stand for whether user has read this notification
 *
 */
@property (nonatomic, assign, getter = isAlreadyRead, setter = setAlreadyRead:) BOOL alreadyRead;
#pragma mark - Methods
- (id) initWithPayload:(NSDictionary*)payload;

- (id) initWithTitle:(NSString*)title
			 message:(NSString*)message;

@end
