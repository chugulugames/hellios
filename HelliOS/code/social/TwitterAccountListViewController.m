//
//  TwitterAccountListViewController.m
//  HelliOS
//
//  Created by Sergio Kunats on 3/19/12.
//  Copyright (c) 2012 Chugulu. All rights reserved.
//

#import "TwitterAccountListViewController.h"
#import "DDSocialDialog.h"
#import "TwitterViewController.h"

@interface TwitterAccountListViewController ()

@end

@implementation TwitterAccountListViewController

@synthesize onTwitterAccountSelect;

- (id)initWithAccounts:(NSArray *)accounts
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self)
        _accounts = [accounts retain];
    return self;
}

- (void)dealloc
{
    [_accounts release];
    self.onTwitterAccountSelect = nil;
    [super dealloc];
}

- (void)show {
    [(DDSocialDialog *)(self.view) show];
}

- (void)loadView
{
    DDSocialDialog* dialog = [[DDSocialDialog alloc] initWithFrame:CGRectMake(0.0, 0.0, WIDTH, HEIGHT) theme:DDSocialDialogThemeTwitter];
    dialog.dialogDelegate  = self;
    dialog.titleLabel.text = NSLocalizedString(@"HO_TWITTER_TITLE", nil);
    UITableView* tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - MARGIN * 4 - 2, HEIGHT - MARGIN * 5 - 15 -2) style:UITableViewStylePlain];
    [dialog.contentView addSubview:tableView];
    
    tableView.delegate   = self;
    tableView.dataSource = self;
    self.tableView = tableView;
    [tableView release];
    self.view = dialog;
    self.view.autoresizesSubviews = YES;
    [dialog release];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (_accounts == nil ? 0 : [_accounts count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    ACAccount *account        = [_accounts objectAtIndex:[indexPath row]];
    cell.textLabel.text       = account.username;
    cell.detailTextLabel.text = account.accountDescription;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.onTwitterAccountSelect)
        self.onTwitterAccountSelect([_accounts objectAtIndex:indexPath.row]);
    [(DDSocialDialog *)(self.view) dismiss:YES];
}

- (void)socialDialogDidCancel:(DDSocialDialog *)socialDialog {
    [self release];
}

@end
