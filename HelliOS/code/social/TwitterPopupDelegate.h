/*******************************************************************************
 *  @file		TwitterPopupDelegate.h
 *  @brief		HelliOS 
 *  @author		François Benaiteau
 *  @version	1.0
 *  @date		9/14/11
 *
 *  Copyright 	Chugulu Games 2009-2011. All rights reserved.
 *******************************************************************************/

#import <Foundation/Foundation.h>



@protocol TwitterPopupDelegate <NSObject>


- (void) shouldOpenAuthorizationDialog:(UIViewController*)controller;

@optional

@end
