/*******************************************************************************
 *  @file		I18n.h
 *  @brief		HelliOS 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/25/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOLocalizationWrapper.h"