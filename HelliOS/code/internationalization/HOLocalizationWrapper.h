/**
 @file		HOLocalizationWrapper.h
 @brief		HOLocalizationWrapper 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
 
 Load Controller and its associated view (if exists)
 from a nib file
*/

#import <Foundation/Foundation.h>


@interface HOLocalizationWrapper : NSObject {}

/**
 @brief this method force the current localization
 @param NSString *localization : The localization to force
 @note  set to nil to use current default localization
 */
+ (void)		setLocalization:(NSString*)localization;

/**
 @brief this method return the current forced localization
 @note  return nil if localization is not forced;
 */
+ (NSString*)	localization;

/**
 @brief this method return the current bundle witch depends on the current localization
 @note  return [NSBundle mainBundle] if localization is not forced;
 */
+ (NSBundle*)	bundle;

/**
 @brief  this method explore the content of a given view reference and localize its components
 @param  UIView	  *view  : The view to localize
 */
+ (void)		localizeView:(UIView*)view;

/**
 @brief  this method localize a string
 @param  NSString *string  : The string to localize
 */
+ (NSString*)	localizedString:(NSString*)key;

/**
 @brief  this method localize an image using the current localization
 @param  NSString *imageName  : The name of the image
 */
+ (UIImage*)	localizedImage:(NSString *)imageName;

@end
