//
//  LocalizationHelper.m
//  blind_test
//
//  Created by Olivier THIERRY on 3/2/11.
//  Copyright 2011 Chugulu. All rights reserved.
//

#import "HOLocalizationWrapper.h"

static NSString *localizationIdentifier = nil;

@implementation HOLocalizationWrapper

+ (void)			setLocalization:(NSString*)localization {
	localizationIdentifier = localization;
}

+ (NSString*)		localization {
	return localizationIdentifier;
}

+ (NSBundle*)		bundle {
	if ([self localization])
		return [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[self localization] ofType:@"lproj"]];
	return [NSBundle mainBundle];
}

+ (NSString*)		localizedString:(NSString*)key {
	NSString *retVal = NSLocalizedStringFromTableInBundle(key, nil, [self bundle], nil);
	if (!retVal || [retVal isEqualToString:key])
		retVal		 = NSLocalizedStringFromTableInBundle(key, nil, [NSBundle mainBundle], nil);

	return retVal == nil ? key : retVal;
}

+ (UIImage*)		localizedImage:(NSString *)imageName {
	
	NSRange extentionRange		= [imageName rangeOfString:@"." options:NSBackwardsSearch];
	
    if (extentionRange.location == NSNotFound) {
        return nil;
    }
    
	NSString *epuredImageName	= [imageName substringToIndex:extentionRange.location];
	NSString *extention			= [imageName substringFromIndex:extentionRange.location + 1];
	
	/**
		@TODO
		if (![allowedExtentions contains:extention])
		 => Raise an Exception
	 */
	
	// Check if  the localization is not forced or if the current forced localization bundle
	// can not find the dedired resource
	if (![self localization] || ![[self bundle] pathForResource:epuredImageName ofType:extention]) {
		return [UIImage imageNamed:imageName];
	}

	return [UIImage imageWithContentsOfFile:[[self bundle] pathForResource:epuredImageName ofType:extention]];
}

+ (void)			localizeView:(UIView*)view {
	for (UIView* subview in view.subviews) {
        if ([subview isKindOfClass:[UILabel class]] || [subview isKindOfClass:[UITextView class]])
            [subview setValue:[self localizedString:[subview valueForKey:@"text"]] forKey:@"text"];
        else if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*)subview;

            [button setTitle:[self localizedString:[button titleForState:UIControlStateNormal]] forState:UIControlStateNormal];
            /*
             Disable for the moment due to some causes

            [button setTitle:[self localizedString:[button titleForState:UIControlStateApplication]] forState:UIControlStateApplication];
            [button setTitle:[self localizedString:[button titleForState:UIControlStateDisabled]] forState:UIControlStateDisabled];
            [button setTitle:[self localizedString:[button titleForState:UIControlStateHighlighted]] forState:UIControlStateHighlighted];
            [button setTitle:[self localizedString:[button titleForState:UIControlStateReserved]] forState:UIControlStateReserved];
            [button setTitle:[self localizedString:[button titleForState:UIControlStateSelected]] forState:UIControlStateSelected];	
             */
        }
        else
            [self localizeView:subview];// Recursive call. We explore each subviews of each view
    }
}

@end
