#!/bin/bash 

set -o errexit
set -o nounset

xcodebuild -project HelliOS.xcodeproj -target HelliOS -configuration $BUILD_STYLE -sdk iphonesimulator
xcodebuild -project HelliOS.xcodeproj -target HelliOS -configuration $BUILD_STYLE -sdk iphoneos

rm -rf tmp/HelliOS_Universal
mkdir tmp/HelliOS_Universal
cp -r tmp/$BUILD_STYLE-iphoneos/HelliOS.framework tmp/HelliOS_Universal

rm -rf tmp/HelliOS_Universal/HelliOS.framework/HelliOS

lipo -create tmp/$BUILD_STYLE-iphoneos/HelliOS.framework/HelliOS tmp/$BUILD_STYLE-iphonesimulator/HelliOS.framework/HelliOS -output tmp/HelliOS_Universal/HelliOS.framework/HelliOS
